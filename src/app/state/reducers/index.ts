import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { counterReducer } from '../counter.reducer';
import { totalReducer } from '../total/total.reducer';
import { sortReducer } from '../sort/sort.reducer';

export interface State {
}

export const reducers: ActionReducerMap<State> = {
  count: counterReducer,
  total: totalReducer,
  sort: sortReducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
