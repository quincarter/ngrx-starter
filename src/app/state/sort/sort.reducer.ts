import { SortAction } from '../../models/tools';
import { initialState } from '../total/total.reducer';
import { SortActions } from './sort.actions';
import { moveItemInArray } from '@angular/cdk/drag-drop';

export function sortReducer(state = initialState, action: SortAction) {
    switch (action.type) {
        case SortActions.SortTools:
            moveItemInArray(action.list, action.previousIndex, action.currentIndex);
            return state;
        case SortActions.SortCart:
            moveItemInArray(action.list, action.previousIndex, action.currentIndex);
            return state;
    }
}
