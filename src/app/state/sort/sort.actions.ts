import { Action } from '@ngrx/store';
import { DropLocation, ITools, SortAction } from '../../models/tools';
import { CdkDragSortEvent } from '@angular/cdk/drag-drop';

export enum SortActions {
  SortTools = '[Sort] Sort Tools List',
  SortCart = '[Sort] Sort Cart List',
}

export enum DropLocations {
  Cart = 'CartDropList',
  Tools = 'ToolsDropList'
}

export class SortToolsList implements Action, SortAction {
  readonly type: string;
  readonly list: ITools[];
  readonly previousIndex: number;
  readonly currentIndex: number;

  constructor(event: CdkDragSortEvent<ITools[]>, dropLocation: DropLocation) {
    switch (dropLocation) {
      case 'ToolsDropList':
        this.type = SortActions.SortTools;
        break;
      case 'CartDropList':
        this.type = SortActions.SortCart;
        break;
      default:
        this.type = null;
        break;
    }
    this.list = event.container.data;
    this.previousIndex = event.previousIndex;
    this.currentIndex = event.currentIndex;
  }
}
