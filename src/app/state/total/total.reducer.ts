import { TotalActions } from './total.actions';
import { ISavedState, ToolsAction } from '../../models/tools';

export const initialState: ISavedState[] = [ {
    tools: [
        {
            name: 'SID 2-A',
            price: 30.01
        },
        {
            name: 'SID 4-A22',
            price: 20.69
        },
        {
            name: 'SID 8-A22',
            price: 30.42
        },
        {
            name: 'SC 60W-A36',
            price: 33.55
        },
        {
            name: 'SCM 22-A',
            price: 87.88
        },
        {
            name: 'SCW 22-A',
            price: 99.99
        },
        {
            name: 'Battery pack B 36/5.2',
            price: 25.89
        },
    ],
    cart: [],
    total: 0.00,
} ];

export function totalReducer(state = initialState, action: ToolsAction) {
    switch (action.type) {
        case TotalActions.AddTools:
            state[0].total += parseFloat(action.cart[action.itemAddedIndex].price.toFixed(2));
            return state;

        case TotalActions.RemoveTools:
            state[0].total -= parseFloat(action.tools[action.itemRemovedIndex].price.toFixed(2));
            return state;

        case TotalActions.SaveState:
            const savedCart = [ {
                tools: action.tools,
                cart: action.cart,
                total: parseFloat(state[0].total.toFixed(2))
            } ];

            localStorage.setItem('currentState', JSON.stringify(savedCart));
            return state;

        case TotalActions.LoadState:
            if (localStorage.getItem('currentState')) {
                state = JSON.parse(localStorage.getItem('currentState'));
            }
            return state;

        default:
            return state;
    }
}
