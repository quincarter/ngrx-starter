import { Action } from '@ngrx/store';
import { ISavedState, ITools, ToolsAction } from '../../models/tools';

export enum TotalActions {
    AddTools = '[Total Component] Add Tools',
    RemoveTools = '[Total Component] Remove Tools',
    SaveState = '[Total Component] Save Current State',
    LoadState = '[Total Component] Load Saved State',
}

export class AddTools implements Action, ToolsAction {
    readonly tools: ITools[];
    readonly cart: ITools[];
    readonly itemAddedIndex: number;
    readonly type = TotalActions.AddTools;

    constructor(
        tools: ITools[],
        cart: ITools[],
        itemAddedIndex: number
    ) {
        this.tools = tools;
        this.cart = cart;
        this.itemAddedIndex = itemAddedIndex;
    }
}

export class RemoveTools implements Action, ToolsAction {
    readonly tools: ITools[];
    readonly cart: ITools[];
    readonly itemRemovedIndex: number;
    readonly type = TotalActions.RemoveTools;

    constructor(
        tools: ITools[],
        cart: ITools[],
        itemRemovedIndex: number
    ) {
        this.tools = tools;
        this.cart = cart;
        this.itemRemovedIndex = itemRemovedIndex;
    }
}

export class SaveState implements Action, ToolsAction {
    readonly tools?: ITools[];
    readonly cart?: ITools[];
    readonly type = TotalActions.SaveState;

    constructor(
        state: ISavedState[]
    ) {
        this.tools = state[0].tools;
        this.cart = state[0].cart;
    }
}

export class LoadState implements Action, ToolsAction {
    readonly type = TotalActions.LoadState;
}

