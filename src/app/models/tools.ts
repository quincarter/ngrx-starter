export interface ITools {
    name: string;
    price: number;
}

export interface ToolsAction {
    type: string;
    tools?: ITools[];
    cart?: ITools[];
    itemAddedIndex?: number;
    itemRemovedIndex?: number;
}

export interface SortAction {
    type: string;
    list: ITools[];
    previousIndex: number;
    currentIndex: number;
}

export interface ISavedState {
    tools: ITools[];
    cart: ITools[];
    total?: number;
}

export declare type DropLocation = 'ToolsDropList' | 'CartDropList';
