import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { AddTools, LoadState, RemoveTools, SaveState } from '../../state/total/total.actions';
import { DropLocation, ISavedState, ITools } from '../../models/tools';
import { SortToolsList } from '../../state/sort/sort.actions';
import { initialState } from '../../state/total/total.reducer';

@Component({
    selector: 'app-drag-drop',
    templateUrl: './drag-drop.component.html',
    styleUrls: [ './drag-drop.component.scss' ],
})
export class DragDropComponent implements OnInit {
    savedStateArray = [];

    savedState$: Observable<any>;
    private savedStateSubscription: Subscription;

    constructor(
        private store: Store<ISavedState[]>
    ) {
        this.savedState$ = store.pipe(select('total'));
    }

    ngOnInit(): void {
        this.getCartList();
        this.store.dispatch(new LoadState());
    }

    dropItemLogic(event: CdkDragDrop<ITools[]>, dropLocation: DropLocation): void {
        if (event.previousContainer === event.container) {
            this.sortTools(event, dropLocation);
        } else {
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
            if (dropLocation === 'CartDropList') {
                console.log('event', event);
                const itemAddedIndex = event.currentIndex;
                this.dropCart(event.previousContainer.data, event.container.data, itemAddedIndex
                )
                ;
            } else {
                const itemRemovedIndex = event.currentIndex;
                this.dropToolsList(event.container.data, event.previousContainer.data, itemRemovedIndex);
            }
        }
    }

    private dropCart(tools: ITools[], cartList: ITools[], itemAddedIndex): void {
        this.store.dispatch(new AddTools(tools, cartList, itemAddedIndex));
        this.save(this.savedStateArray);
    }

    private dropToolsList(tools: ITools[], cartList: ITools[], itemRemovedIndex) {
        this.store.dispatch(new RemoveTools(tools, cartList, itemRemovedIndex));
        this.save(this.savedStateArray);
    }

    private sortTools(event: CdkDragDrop<ITools[]>, dropLocation: DropLocation): void {
        this.store.dispatch(new SortToolsList(event, dropLocation));
        this.save(initialState);
        // this.save(this.savedStateArray); @todo - solution to sorting bug
    }

    private save(state: ISavedState[]): void {
        this.store.dispatch(new SaveState(state));
    }

    private getCartList(): void {
        this.savedStateSubscription = this.savedState$.subscribe(data => {
            this.savedStateArray = data;
        });
    }
}
